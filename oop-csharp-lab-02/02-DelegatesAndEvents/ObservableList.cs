﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>
    {
        List<TItem> list = new List<TItem>();
        public IEnumerator<TItem> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            list.Add(item);
            ElementInserted?.Invoke(this, item, list.Count - 1);
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(TItem item)
        {
            return list.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            var ind = list.BinarySearch(item);
            if (list.Remove(item))
            {
                ElementRemoved?.Invoke(this, item, ind);
                return true;
            }
            return false;
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public int IndexOf(TItem item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            list.Insert(index, item);
            ElementInserted?.Invoke(this, item, index);
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
            ElementRemoved?.Invoke(this, list[index], index);
        }

        public TItem this[int index]
        {
            get { return list[index]; }
            set
            {
                var oldval = list[index];
                list[index] = value;
                ElementChanged?.Invoke(this, value, oldval, index);
            }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}