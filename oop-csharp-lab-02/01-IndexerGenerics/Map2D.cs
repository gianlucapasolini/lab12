﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach(var x in keys1)
            {
                foreach(var y in keys2)
                {
                    values.Add(new Tuple<TKey1, TKey2>(x, y), generator.Invoke(x, y));
                }
            }
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            try
            {
                var gino = values.First(g => !(g.Value.Equals(other[g.Key.Item1, g.Key.Item2])));
                return false;
            }
            catch
            {
                return true;
            }
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */
            get
            {
                return values[new Tuple<TKey1, TKey2>(key1, key2)];
            }

            set
            {
                values[new Tuple<TKey1, TKey2>(key1, key2)] = value; 
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            //metodo standard
            var list = new List<Tuple<TKey2, TValue>>();
            foreach(var g in values)
            {
                if (g.Key.Item1.ToString() == key1.ToString())
                {
                    list.Add(new Tuple<TKey2, TValue>(g.Key.Item2, g.Value));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            //metodo con linq
            var list = (from gino in values
                        where gino.Key.Item2.ToString() == key2.ToString()
                        select new Tuple<TKey1, TValue>(gino.Key.Item1, gino.Value)).ToList();
            return list;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            var list = (from gino in values
                        select new Tuple<TKey1, TKey2, TValue>(gino.Key.Item1, gino.Key.Item2, gino.Value)).ToList();
            return list;
        }

        public int NumberOfElements
        {
            get
            {
                return values.Count;
            }
        }

        public override string ToString()
        {
            var stampe = values.Select(g => "key1: " + g.Key.Item1 + "; key2: " + g.Key.Item2 + "; val: " + g.Value)
                               .ToList();
            return string.Join(Environment.NewLine, stampe.ToArray());
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
